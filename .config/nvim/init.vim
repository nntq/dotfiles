"""" The basics
let mapleader =" "
syntax on

set number
set tabstop=2
set softtabstop=-1
set shiftwidth=0
set expandtab
set listchars=space:␣,tab:>· nolist
set mouse=
set clipboard=unnamed
set nohlsearch
set nobackup
set laststatus=2 " For lightline

"""" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
set splitbelow splitright

"""" Simplify split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

"""" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"""" Evaluate init.vim on save
autocmd BufWritePost stdpath('config').'/init.vim' so %

"""" Load plugins
call plug#begin(stdpath('data') . '/plugged')
Plug 'itchyny/lightline.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'justinmk/vim-sneak'
call plug#end()

"""" FZF
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } }
map <Leader><Leader> :FZF<CR>
map <Leader>el :vsplit<CR>:FZF<CR>
map <Leader>ej :split<CR>:FZF<CR>

"""" Motion
map æ <Plug>Sneak_,
map ø <Plug>Sneak_;
