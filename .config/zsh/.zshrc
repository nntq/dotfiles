export ZSH="/home/steen/.local/src/oh-my-zsh"
ZSH_THEME="robbyrussell"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to automatically update without prompting.
DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(git zsh-completions zsh-autosuggestions zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

# User configuration
# History
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=10000

# Settings
setopt appendhistory autocd nomatch notify
unsetopt beep extendedglob
bindkey -e # Keybindings: -v for VI, -e for EMACS
bindkey '^R' history-incremental-search-backward
CASE_SENSITIVE="false"

# Auto-completion
zstyle :compinstall filename "$HOME/.config/zsh/.zshrc"
autoload -Uz compinit
compinit -d "$HOME/.cache/zsh/zcompdump-$ZSH_VERSION"

# Add things to path
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/.local/npm/bin"
export PATH="$PATH:$HOME/.config/composer/vendor/bin"

# Set favorite programs
export BROWSER="firefox"
export EDITOR="nvim"
export TERM="termite"

# Set window title
case $TERM in
    st*|xterm*|termite*)
        precmd () { print -Pn "\e]0;%~\a" } ;;
esac

# Set prompt
PROMPT="[%B%F{green}%n%f%b@%m %~]$ "

# User specific aliases and functions
alias vim='nvim'
alias mutt='neomutt'
alias la='ls -al'
alias ssh='env TERM=xterm ssh'
alias shuttle="sshuttle @$HOME/.config/sshuttle.conf"
alias hs='cd ~/Homestead && vagrant up && vagrant ssh'
alias dig='dig +noall +answer'
alias netctl="sudo netctl"
alias forget='ssh-keygen -R'
alias clip='xclip -in -selection clipboard'
alias deccert='openssl x509 -text -noout -in'
alias userctl='systemctl --user'
alias vihosts='sudo vim /etc/hosts'
alias swapiso='xmodmap -e "keycode 49 = less greater less greater backslash"'
alias restic="source '$HOME/.config/restic/credentials' && restic -r s3:https://s3-dk6.clu1.obj.storagefactory.io/callisto-backup"
alias fixmon="autorandr -c home; autorandr -c work"
alias fixkeys='xset s off; xset r rate 150 40; setxkbmap -option caps:escape; xmodmap -e "keycode 49 = less greater less greater backslash"'

# Bluetooth commands
alias bt-scan='bluetoothctl devices'
alias bt-connect='bluetoothctl connect'
alias bt-disconnect='bluetoothctl disconnect'
alias bt-headphones='bluetoothctl connect 00:0A:45:0B:1C:4A'
alias bt-on='bluetoothctl power on'
alias bt-off='bluetoothctl power off'

# Shortcuts
source "$HOME/.config/zsh/shortcuts"

# Git aliases
alias gca='git add . && git commit -m'
alias gs='git status'
alias gd='git diff'
alias gp='git push'
alias gc='git checkout'
alias gcb='git checkout -b'

# Fix some programs to use XDG
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"

export HISTFILE="$XDG_DATA_HOME/zsh/history"
export LESSHISTFILE="$XDG_CACHE_HOME/less/history"
export LESSKEY="$XDG_CONFIG_HOME/less/lesskey"
export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"
export GTK_RC_FILES="$XDG_CONFIG_HOME/gtk-1.0/gtkrc"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export MYSQLHISTFILE="$XDG_DATA_HOME/mysql/history"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME/java"
export NOTMUCH_CONFIG="$XDG_CONFIG_HOME/notmuch/notmuch"
export NVM_DIR="$XDG_CONFIG_HOME/nvm"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export VAGRANT_HOME="$XDG_DATA_HOME/vagrant"
export VAGRANT_ALIAS_FILE="$XDG_DATA_HOME/vagrant/aliases"
export PASSWORD_STORE_DIR="$XDG_CONFIG_HOME/password-store"

alias wget="wget --hsts-file=\"$XDG_CACHE_HOME/wget-hsts\""

[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

cat "$XDG_CACHE_HOME/zsh/motd.d/"*
echo
